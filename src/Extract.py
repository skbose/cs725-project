#
# Class:      Extract
# Author:     skbose
# Created:    Nov 7th 2016
#
# Description:
#
# @ This class will be used to extract hand from any image using skin tone detection along with some classifier.
# @ The input to this class will be a video feed and the output will be boxed out regions which will enclose the hand.
#

import numpy as np
from __builtin__ import staticmethod

import cv2
from core import Globals
from Action import Action
from Webcam import WebCam
import time

class Extract:

    # static variables
    cascadeFileLoc = '../models/cascade.xml'
    hand_cascade = cv2.CascadeClassifier(cascadeFileLoc)

    def __init__(self):
        # do nothing
        return None

    @staticmethod
    def CameraSim():

        """
        @input: None
        :return: None

        Uses opencv to feed live video from the webcam.\
        """
        cap = cv2.VideoCapture(0)
        captureImg = None

        while True:

            # frame by frame capture
            r_, frame = cap.read()
            frame = cv2.flip(frame, 1)

            # does some wierd operation on the frame; probably coverting to gray scale
            grayscale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # display the frame
            rects = Extract.handDetect(grayscale)
            contrastAdjusted = cv2.equalizeHist(grayscale)
            frame = Extract.drawRectangle(frame, rects)

            # contour detection
            # contours, hierarchy = cv2.findContours(contrastAdjusted, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            # cv2.drawContours(frame, contours, -1, (0, 255, 0), 3)

            cv2.imshow(Globals.windowTitle, frame)

            # capture and return the image file
            keyPress = cv2.waitKey(1) & 0xFF

            if keyPress == ord('c'):
                print "Capture!"
                captureImg = gray
                break
            elif keyPress == ord('q'):
                break

        # release resources after breaking from the loop
        cap.release()
        cv2.destroyAllWindows()
        return captureImg

    @staticmethod
    def drawRectangle(img_, rects_):

        if not len(rects_):
            return img_

        for (x, y, w, h) in rects_:
            cv2.rectangle(img_, (x, y), ((x + w), (y + h)), (255, 0, 0), 2)

        return img_

    @staticmethod
    def handDetect(img_):

        rects = Extract.hand_cascade.detectMultiScale(img_, 3, 10)

        if not len(rects):
            print "No hands found!"
            return []
        else:
            print "Yay! Hands."

        return rects

if __name__ == '__main__':
    print "Starting live feed."
    # Extract.handDetect()
    Extract.CameraSim()
    # cam = WebCam()
    # Action.doubleKeyPressWithAsyncTimer("alt", "f4", 3)
    # Action.singleKeyPressWithAsyncTimer("enter", 5)
    # cam.captureImgFromVid()
    print "Exiting program."

# This README file will be used to document any significant changes or issues to look into.
# Proposed Project: Hand Gesture Recognition System
-----------------------------
Moving Forward: Installations
-----------------------------

# (1) https://milq.github.io/install-opencv-ubuntu-debian/

---------------
Relevant Papers:
----------------

# (1) http://airccse.org/journal/iju/papers/3112iju03.pdf
# (2) http://research.microsoft.com/en-us/um/people/awf/bmvc02/project.pdf
# (3) http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.454.3689&rep=rep1&type=pdf
# (4) http://www.rroij.com/open-access/hand-gesture-recognition--analysis-ofvarious-techniques-methods-and-theiralgorithms.pdf
# (5) http://www.ics.forth.gr/indigo/pdf/chapter_gestures_submitted.pdf


#Useful Links

# (1) https://www.intorobotics.com/9-opencv-tutorials-hand-gesture-detection-recognition/
# (2) http://www.codeproject.com/Articles/26280/Hands-Gesture-Recognition
# (3) http://research.ijcaonline.org/volume71/number15/pxc3889123.pdf -- this is good. Gives a short description of a lot of methods and also links to study further.

